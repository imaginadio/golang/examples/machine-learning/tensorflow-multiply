module gitlab.com/imaginadio/golang/examples/machine-learning/tensorflow-multiply

go 1.20

require github.com/wamuir/graft v0.3.0

require google.golang.org/protobuf v1.28.1 // indirect
